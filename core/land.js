/**
 * Created by edgera on 10/13/2014.
 */

/*
 #552233
 #bb6666
 #ff8844
 #ffeecc
 #ffdd77

 #443333
 #997777
 #cc9977
 #eeeedd
 #eecc99
 */

function Land(player){
    this.player = player;
    this.life = 10;
    this.gold = 200;
    this.income = 0;
    this.creeps = [];
    this.towers = [];
}


module.exports = Land;