/**
 * Created by edgera on 9/30/2014.
 */


function Engine() {
}
/*
 * tick will update the gamestate that is provided with whatever happens after 1 tick
 * @args gamestate is an stage_by_player json object, which each stages having game objects.
 */
Engine.tick = function(gamestate) {
    // 0. activate creeps
    for (var i = 0; i < gamestate.players.length; i++) {
        var player = gamestate.players[i];
        var creeps = gamestate.lands[player].creeps;
        creeps.forEach(function(creep){
            creep.suspend_timer = creep.suspend_timer>0?creep.suspend_timer-1 :creep.suspend_timer;
        });

    }
    // 1. check if any creeps have died
    // 2. update creep positions
    var map = gamestate['map'];

    for (var i = 0; i < gamestate.players.length; i++) {
        var player = gamestate.players[i];
        var creeps = gamestate.lands[player].creeps;
        creeps.forEach(function(creep){
            if(creep.suspend_timer==0) {
                creep.updateDelta(1, map['path']);
            }
        });
    }
    // 2.a if creeps complete land, move them to the next land and doc player's hp.
    for (var i = 0; i < gamestate.players.length; i++) {
        var player = gamestate.players[i];
        var land = gamestate.lands[player];
        var creeps = gamestate.lands[player].creeps;
        creeps.forEach(function(creep){
            if(creep.isFinishedOnLand) {
                console.log('creep reached end of land, does ' + creep.attack_damage + ' damage.');
                land.life -= creep.attack_damage;
            }
        });
    }

    // 3. update tower targets (eg re target if creeps out of range)
    // 4. affect the creeps
    // TODO sticky targeting
    for (var i = 0; i < gamestate.players.length; i++) {
        var player = gamestate.players[i];
        var towers = gamestate.lands[player].towers;
        var creeps = gamestate.lands[player].creeps;

        towers.forEach(function(tower){
            tower.shootAt(creeps);
        });
    }
    // 5. remove dead or completed creeps. cleanup
    for (var i = 0; i < gamestate.players.length; i++) {
        var player = gamestate.players[i];
        var creeps = gamestate.lands[player].creeps;
        var remaining_creeps = [];
        creeps.forEach(function(creep){
            if(creep.health <1){
                creep.removeSprite();
                return;
            }

            // TODO if creep.isFinishedOnLand && nextLand is home land. remove sprite and pass to next
            if(creep.isFinishedOnLand ){
                creep.removeSprite();
                return;
            }

            remaining_creeps.push(creep);

        });
        gamestate.lands[player].creeps = remaining_creeps;
    }

    return gamestate;
};


Engine.isWaveFinished = function(gamestate){
    for (var i = 0; i < gamestate.players.length; i++) {
        var player = gamestate.players[i];
        var creeps = gamestate.lands[player].creeps;
        for ( var j = 0 ; j < creeps.length; j ++){
            //check if any not finished.
            if(creeps[j].isFinishedOnLand==false && creeps[j].health>0){
                return false;
            }
        }
    }
    return true;
};

Engine.numberOfRemainingLands = function(gamestate){
    var count = 0;
    for (var i = 0; i < gamestate.players.length; i++) {
        var player = gamestate.players[i];
        var life = gamestate.lands[player].life;
        console.log('player:' + player + ' hp:' + life);
        if (life > 0) {
           count++;
        }
    }
    return count;
}

Engine.is_legal_buy = function(asset, player, gamestate){
    var available_gold = gamestate.lands[player].gold;
    // Check if player can afford the asset.
    if(asset.cost > available_gold){
        return false;
    }

    //TODO any additional rule checking
    return true; // if all checks pass, it is a legal purchase.
}

/*
 * buy_creep checks if the creepbuy is legal before updating the gamestate.
 * - deducts creeps cost from player's gold
 * - adds creep to receiving player's list of creeps.
 *
 * we do gamestate maintainance in the engine to maintain rules consistency across client and server.
 */
Engine.buy_creep = function(creep,purchasing_player,gamestate){
    if(!Engine.is_legal_buy(creep, purchasing_player, gamestate)){
        console.log('illegal buy creep not purchased');
        return false; // TODO reason why unsucessful.
    }

    //find recipient of the creep.
    var players = gamestate.players;
    for(var i = 0; i < players.length; i ++){
        var player = players[i];
        if(player==purchasing_player){
            break;//we found the player's land & the player's index
        }
    }
    var target_player = players[(i+1)%players.length]; //pass to next player's land
    gamestate.lands[target_player].creeps.push(creep);
    gamestate.lands[purchasing_player].gold -= creep.cost;
    gamestate.lands[purchasing_player].income += creep.income;
    console.log(gamestate.lands[purchasing_player].income);

    console.log('in engine ' +gamestate.lands[purchasing_player].gold);
    //TODO increase income for purchasing player
    return true;
};


Engine.buy_tower = function(tower,purchasing_player,gamestate){
    if(!Engine.is_legal_buy(tower, purchasing_player, gamestate)){
        console.log('illegal buy tower not purchased');
        return false; // TODO reason why unsucessful.
    }
    gamestate.lands[purchasing_player].towers.push(tower);
    gamestate.lands[purchasing_player].gold -= tower.cost;

    return true;
};

Engine.do_end_of_wave = function(gamestate){
    var players = gamestate.players;
    var lands = gamestate.lands;
    for(var i = 0; i < players.length; i ++){
        var player = players[i];
        var land = lands[player];
        land.gold += land.income;
        console.log(land.income);
    }

}




// export the class
module.exports = Engine;