
//if this gamestate.js file is being used by nodejs server
if (typeof require != 'undefined'){
    var Land = require('./land');
    var Creep = require('./creep');
    var Tower = require('./tower');
}

function Gamestate(players,map){
    if(arguments.length == 0){
        this.players = []; // array of player ids
        this.lands = {} ; // map of player->land object
        this.map = {} ;
    }
    if(arguments.length == 2){
        this.players = players; // array of player ids
        this.lands = {};// map of player->land object
        for (var i = 0; i<players.length; i++) {
            var player = players[i];
            var land = new Land(player);
            this.lands[player] = land;
        }
        this.map = map;
    }
};


Gamestate.prototype.toJson = function(){
    var json_gamestate = {   };
    json_gamestate['lands']={};
    json_gamestate['map'] = this.map;
    json_gamestate['players']=this.players;

    for(var i = 0; i < this.players.length; i++){
        var player = this.players[i];
        var land = this.lands[player];
        console.log('player:'+player+' - gold:' + land.gold);
        json_gamestate.lands[player] = {
            player:player,
            income:land.income,
            life:land.life,
            gold:land.gold,
            towers:land.towers,
            creeps:land.creeps
        };
    }
    console.log('finished building');
    return json_gamestate;
};

Gamestate.fromJson = function(json_gamestate){
    var map = json_gamestate['map'];
    var players = json_gamestate['players'];

    var gamestate = new Gamestate();
    gamestate.map = map;
    gamestate.players = [];
    gamestate.lands = {};

    for (var i = 0; i<players.length; i++) {
        var player = players[i];
        gamestate.players.push(player);

        var json_land = json_gamestate.lands[player];
        var land = new Land(player);

        var creeps_json =   json_land.creeps;
        for(var j = 0; j < creeps_json.length; j ++){
            var obj = creeps_json[j];
            var creep = new Creep(obj.speed,obj.health,stages[player]);
            creep.suspend_timer=j*50; //TODO variable creep spawn rate
            land.creeps.push(creep);
        }
        var towers_json =  json_land.towers;

        for(var j = 0; j < towers_json.length; j ++){
            var obj = towers_json[j];
            var tower = new Tower(obj.x,obj.y,stages[player]);
            land.towers.push(tower);
        }

        land.life =   json_land.life;
        land.gold =   json_land.gold;
        land.income =   json_land.income;

        gamestate.lands[player] = land;
    }
    return gamestate;
};

module.exports = Gamestate;