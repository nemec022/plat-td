/**
 * Created by edgera on 9/30/2014.
 */
// potential creeps
var CreepEnum = Object.freeze(
    {
        GENERIC: Creep,
        GREEN: 1,
        BLUE: 2
    }
);


// Constructor
function Creep(speed, health, stage) {
    //init sprites off the screen for now.

    this.type='GENERIC';

    this.x = -100;
    this.y = -100;
    this.speed = speed;
    this.health = health;
    this.attack_damage = 1;

    this.cost = 100;
    this.income = 50;

    this.travel_distance = 0;
    this.suspend_timer = 0;
    this.isFinishedOnLand = false

    if (stage != null) {
        this.sprite = this.create_sprite(stage);
        this.stage = stage;
    }
}
// class methods
Creep.prototype.create_sprite = function(target_stage) {
    //nodejs dont have no createjs nonsense visualizations.
    if (typeof createjs === 'undefined') {
        return {};
    }

    var circle = new createjs.Shape();
    circle.graphics.beginFill("blue").drawCircle(0, 0, grid_height/2);
    circle.x = this.x * grid_width;
    circle.y = this.y * grid_height;
    target_stage.addChild(circle);
    return circle;
};

Creep.fromJson = function(json){
    return new Creep(json.speed,json.health);
};

Creep.prototype.removeSprite = function(){
    if(this.stage){
        this.stage.removeChild(this.sprite);
    }
};

Creep.prototype.updateDelta = function(ticks,path) {
    this.travel_distance += ticks * this.speed;

    //figure out how far along path the creep has gotten.
    var potential_travel = 0, traveled = 0;
    var x0, y0, x1, y1;

    for (var i = 0; i < path.length - 3; i += 2) {
        x0 = path[i];
        y0 = path[i + 1];
        x1 = path[i + 2];
        y1 = path[i + 3];
        potential_travel = Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
        if (traveled + potential_travel > this.travel_distance) {
            break;
        }
        traveled += potential_travel;
    }
    //check if creep has left the path.
    if (i + 2 >= path.length) {
        this.isFinishedOnLand = true;
        return; //dont draw.
    }

    //#usetrig
    var residual_distance = this.travel_distance - traveled;
    var ratio = residual_distance / potential_travel;

    this.x = x0 + (ratio * (x1 - x0));
    this.y = y0 + (ratio * (y1 - y0));

    //visualization
    if (typeof this.sprite != 'undefined') {
        this.sprite.x = Math.round(this.x * grid_width);
        this.sprite.y = Math.round(this.y * grid_height);
    }

};

// we only need to know the creep's type to send it on the network.
Creep.prototype.toJson = function(){
    return { type:this.type  };
}

//TODO this isn't done yet.
Creep.fromJson = function(json){
    console.log('creating creep from jsontyp:'+json.type);
    return new CreepEnum[json.type](.1,100);
}


// export the class
module.exports = Creep;