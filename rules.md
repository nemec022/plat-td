**when will the client get updates from the server**
# at the beginning of a wave the server will tell all players what is coming in the wave and what towers the oponents have.


**when will the client sig the server**
# when a tower is purchased/sold.
# when a creep is purchased/sold.


**how does a client interact with the gamestate**
# The client maintains a bunch of 'full' objects which know everything form position to how to draw.
# upon tower purchase, the client draws their tower immediately / adds to their 'full' gamestate.
## the client converts the tower purchase to json descriptor and sends to server.
## server reinstantiates 'full' instance (if valid purchase).
# upon creep purchase, the client draws the queued creep above their oponent's board.
## client converts the creep purchase to json descriptor and sends to server.
## server reinstantiates 'full' instance (if valid purchase).
# when the server decides it is wave time. The board locks ( no new tower placments)
## server converts full gamestate to json, sends to client
## client converts to full gamestate, draws enemy's purchases.
## server and client simulate game events.